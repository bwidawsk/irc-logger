Very simple IRC logger that writes to a sqlite3 database, and raw HTML.
Also incuded is a basic set of HTML/js to dynamically display the results.

It contains a snapshot of [SQL.js](https://github.com/kripken/sql.js) which is
MIT licensed.

TODO:
Add sortable and prettier HTML tables.