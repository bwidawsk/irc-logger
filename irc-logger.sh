#!/bin/bash

readonly host=chat.freenode.net
readonly port=6667
readonly nick=EN-logger
readonly channel=#engineers-nightmare
readonly html_output=/srv/http/engineers-nightmare-raw.html
readonly database=/srv/http/engineers-nightmare.db

# Debug:
#html_output=/dev/tty
#database=~/test.db

#stolen from https://github.com/clsr/sedbot/blob/master/sedbot.bash
readonly user_regex='^\([^!]\+\)!\([^@]\+\)@\(.*\)$'
readonly message_regex='^\(:\([^ ]\+\)\ \)\?\([A-Z0-9]\+\)\( \([^:]\+\)\)\?\( \?:\([^\r\n]*\)\)\?\([\r\n]*\)\?$'


function do_quit() {
        echo -ne "QUIT :Ugh, be right back...\r\n" >&3;
}
trap do_quit INT EXIT


connect() {
	set -e
	exec 3<>/dev/tcp/$host/$port
	echo -ne "NICK $nick\r\n" >&3
	echo -ne "USER $nick $nick $nick :The IRC logger for Engineer's Nightmare (bwidawsk.net/engineers-nightmare.html)\n" >&3
	echo -ne "JOIN $channel\r\n" >&3
	set +e
}

init_db() {
	if [ ! -f $database ] ; then
		sqlite3 $database  "create table EN_log (id INTEGER PRIMARY KEY,Date DATE, Nick TEXT,Message TEXT, Type TEXT);"
	fi
}


connect
init_db

while read line <&3; do
        user=$(echo "$line" | sed "s/$user_regex/\1/g")
        type=$(echo "$line" | sed "s/$message_regex/\3/g")
        msg=$(echo "$line" | sed "s/$message_regex/\6/g")

        # Must respond to pings (every 240s on freenode). Else you'll get ping timeout'd
        case "$line" in
        PING*)
            echo -ne "PONG ${line##PING }" >&3
            ;;
        esac

        case "$user" in
        *.freenode.net*)
                # Maybe we should actually log these?
                echo -ne "$line" > /dev/null
                ;;
        *)
		case "$type" in
		PRIVMSG)
			user=${user#":"}
			msg=${msg#":"}
			date=$(date -u)
			echo -e $date "$user $msg<br>" >> $html_output

			# super annoying but we can't have a ' in any field. This
			# should do the proper replace and not clobber multiple '
			user=$(sed 's/'\''/&&/g' <<< "$user")
			msg=$(sed 's/'\''/&&/g' <<< "$msg")
			sqlite3 $database "insert into EN_log (Date,Nick,Message,Type) values ('$date','$user','$msg','$type');"
			;;
		esac
		;;
        esac

done
